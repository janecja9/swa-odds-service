FROM maven:latest
LABEL authors="kraus"

WORKDIR /app

COPY pom.xml .

COPY src ./src

RUN mvn clean package

CMD ["java", "-jar", "target/odds-service-1.0.0.jar"]

