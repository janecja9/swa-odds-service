package cz.cvut.fel.swa.oddsservice;

import cz.cvut.fel.swa.oddsservice.model.Odds;
import cz.cvut.fel.swa.oddsservice.repository.OddsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OddsServiceApplication {

	private static final Logger log = LoggerFactory.getLogger(OddsServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(OddsServiceApplication.class, args);
	}


	@Bean
	public CommandLineRunner demo(OddsRepository repository) {
		return (args) -> {
			repository.save(new Odds("Barcelona", "Real Madrid", 1.21));
			repository.save(new Odds("PSG", "Bayern", 2.52));
			repository.save(new Odds("Sparta", "Slavie", 1.01));


			Odds odds = repository.findById(2);
			log.info("Bet found with findById(2):");
			log.info("--------------------------------");
			log.info(odds.toString());
			log.info("");

		};
	}
}
