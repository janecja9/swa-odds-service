package cz.cvut.fel.swa.oddsservice.controller;

import cz.cvut.fel.swa.oddsservice.model.Odds;
import cz.cvut.fel.swa.oddsservice.repository.OddsRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OddsController {

    private final OddsRepository oddsRepository;


    @GetMapping("/odds/{oddsId}")
    @Operation(summary = "Get odds by id", description = "Get odds for a particular match by odds-id")
    @ApiResponse(responseCode = "200", description = "Match detail with it's odds")
    @ApiResponse(responseCode = "404", description = "Odds not found", content = @Content)
    public ResponseEntity<Odds> getOddsById(@PathVariable int oddsId) {
        var odds = oddsRepository.findById(oddsId);
        if (odds == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(odds);
        }
    }

    @GetMapping("/odds")
    @Operation(summary= "Get all odds", description = "Get all odds")
    @ApiResponse(responseCode = "200", description = "All odds returned successfully")
    public ResponseEntity<Iterable<Odds>> getAllOdds() {
        return ResponseEntity.ok(oddsRepository.findAll());
    }
}
