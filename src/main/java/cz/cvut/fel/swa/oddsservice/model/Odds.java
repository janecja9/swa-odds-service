package cz.cvut.fel.swa.oddsservice.model;


import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
public class Odds {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String team1;
    private String team2;
    private double odds;

    public Odds(String team1, String team2, double odds) {
        this.team1 = team1;
        this.team2 = team2;
        this.odds = odds;
    }

    public Odds() {
    }


    @Override
    public String toString() {
        return "Odds [id=" + id + ", team1=" + team1 + ", team2=" + team2 + ", odds=" + odds + "]";
    }
}
