package cz.cvut.fel.swa.oddsservice.repository;

import cz.cvut.fel.swa.oddsservice.model.Odds;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OddsRepository extends CrudRepository<Odds, Integer> {

    Odds findById(int id);

}
