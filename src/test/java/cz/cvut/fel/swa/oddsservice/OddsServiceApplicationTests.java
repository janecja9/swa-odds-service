package cz.cvut.fel.swa.oddsservice;

import cz.cvut.fel.swa.oddsservice.controller.OddsController;
import cz.cvut.fel.swa.oddsservice.model.Odds;
import cz.cvut.fel.swa.oddsservice.repository.OddsRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = OddsServiceApplication.class)
class OddsServiceApplicationTests {

	@Autowired
	private OddsRepository repository;

	@Autowired
	private OddsController controller;


	@Test
	@Transactional
	public void saveNewOdds_checkIfSaved() {
		Odds odds = repository.save(new Odds("Barcelona", "Real Madrid", 1.21));

		Odds foundOdds = repository.findById(odds.getId());

		assertNotNull(foundOdds);
		assertEquals(odds, foundOdds);
	}

}
